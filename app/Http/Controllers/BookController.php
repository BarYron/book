<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;




class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        if (Gate::denies('manager')) {
            $boss = DB::table('simples')->where('simple',$id)->first();
            $id = $boss->manager;
        }
        $user = User::find($id);
        $books = $user->books;
        return view('books.Index', compact('books'));
    }
 


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
        abort(403,"Sorry you are not allowed to create books..");}
        return view ('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");}


            $this->validate($request,[
                'title'=>'required',
                'author'=>'required']);
       
 
        $book= new Book();
        $id=Auth::id();
        $book->title = $request->title;
        $book->author = $request->author;
        $book->user_id=$id;
        $book->status=0;
        $book->save();
        return redirect('books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");}
       
        $book = Book::find($id);
        return view(' books.edit',compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'author'=>'required|min:1',
            'updated_at'=>'required|date|before_or_equal:today']);
            
        $book = Book::findOrFail($id);

        if (Gate::denies('manager')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit books..");
        }   

        if(!$book->user->id == Auth::id()) return(redirect('books'));

        $book->update($request->except(['_token']));

        if($request->ajax()){
            return Response::json(array('result' => 'success1','status' => $request->status ), 200);   
        } else {          
            return redirect('books');           
        }
 
       // $todo->update($request->all());
      //  return redirect('todos');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");}

        $book = Book::find($id);
        if (!$book->user->id==Auth::id()) return (redirect('books'));
        
        $book->delete();
        return redirect('books');
    }

    public function rules()
    {
      return ['title' => 'required|unique|max:255',  ];
    }
}
