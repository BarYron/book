@extends('layouts.app')
@section('content')


<h1> This is your book list</h1>
<table>
    <tr>
    <th>status</th>
    <th>Book Name</th>
    <th>Author</th>
    </tr>
    @foreach($books as $book)
    <tr>
    <td> @if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" disabled='disable'  checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}" >
       @endif</td>
    <td>@can('manager')<a href="{{route('books.edit',$book->id)}}"> @endcan{{$book->title}} </a></td>
    <td>{{$book->author}}</td>
    </tr>
    @endforeach
    </table>

    @can('manager') <a href="{{route('books.create')}}"> create a new book </a> @endcan

    <style>
table, th, td {
  border: 1px solid black;
}
</style>

<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
            $(this).attr('disabled', true);
               console.log(event.target.id)
               $.ajax({
                   url: "{{url('books')}}" +'/' + event.target.id,
                   dataType:'json',
                   type: 'put',
                   contentType: 'application/json',
                   data:JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  
@endsection

