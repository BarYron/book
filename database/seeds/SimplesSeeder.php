<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class SimplesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'bar',
            'email' => 'bar@bar.com',
            'password' => Hash::make('12345678'),
            'created_at' => date('Y-m-d G:i:s'),
            'role'=>('simple'),
             ],
             ['name' => 'ziv',
             'email' => 'ziv@ziv.com',
             'password' => Hash::make('12345678'),
             'created_at' => date('Y-m-d G:i:s'),
             'role'=>('simple'),
              ],
         ]);
    }
}
